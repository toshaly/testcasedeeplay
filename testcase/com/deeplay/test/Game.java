package com.deeplay.test;

import java.util.*;

public class Game {
    public static void play(int[] player1, int[] player2, int rolls){
        Random random = new Random();
        int[] result = random.ints(rolls, 1,7).toArray();
        System.out.println(Arrays.toString(result));
        int firstPlayerMatches = matches(result,player1);
        int secondPlayerMatches = matches(result,player2);
        System.out.println("first player " + firstPlayerMatches + " matches" + " second player " + secondPlayerMatches + " matches");
        double chanceBestOf3 = 1.0/216.0;
        double playerChance = (chanceBestOf3 * rolls) / 3;
        double drawChance = playerChance*playerChance;
        System.out.println("draw chance = " + drawChance);

    }

    public static int matches(int[] resultArr, int[] player){
        int matches = 0;
        int subMatches = 0;
        for (int i = 0, j = 0; i < resultArr.length; i++) {
            if (resultArr[i] == player[j]) {
                subMatches++;
                j++;
                if (subMatches == 3){
                    matches++;
                    j = 0;
                    subMatches = 0;
                }
            }else {
                subMatches = 0;
                j = 0;
            }
        }
        return matches;
    }

    public static void fill(int[] arr){
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            arr[i] = scanner.nextInt();
            while (arr[i] > 6 || arr[i] <= 0){
                System.out.println("input number from 1 to 6");
                arr[i] = scanner.nextInt();
            }
        }
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] player1 = new int[3];
        int[] player2 = new int[3];
        System.out.println("first player choose numbers");
        fill(player1);
        System.out.println("second player choose numbers");
        fill(player2);
        System.out.println("how much rolls");
        int playRolls = scanner.nextInt();
        play(player1,player2,playRolls);



    }
}
