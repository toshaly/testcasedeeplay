package com.deeplay.test;

import java.util.Arrays;

import static com.deeplay.test.ArrayTest.bubbleSort;

public class ArraySplit {

    public static boolean arraySplit(int[] arr, int split){
        // use the same algo as in ArrayTest.java
        int arrSum = arrSum(arr);
        if (arrSum % split != 0)
            return false;
        int splitSumArr = arrSum / split;
        bubbleSort(arr,false);
        //find all uniq elements
        int iter = 0;
        int[][] tempResultArr = new int[arr.length][2];
        int[] item = new int[2];
        item[0] = arr[0];
        item[1] = 1;
        tempResultArr[0] = item;
        for (int i = 1; i < arr.length; i++) {
            if(tempResultArr[iter][0] == arr[i]){
                tempResultArr[iter][1]++;
            } else {
                iter++;
                tempResultArr[iter][0] = arr[i];
                tempResultArr[iter][1] = 1;
            }
        }
        int[][] modifyArr = Arrays.copyOfRange(tempResultArr,0, iter+1);
        for (int[] ints : modifyArr) {
            System.out.println(Arrays.toString(ints));
        }
        int arrIter = 0;
        int[][] resultArr = new int[split][arr.length];
        for (int i = 0; i < resultArr.length; i++) {
            int tempSplitSumArr = splitSumArr;
            for (int j = 0; j < resultArr[i].length ; j++){
                while (modifyArr[arrIter][0] > tempSplitSumArr || modifyArr[arrIter][1] == 0)
                    arrIter++;

                resultArr[i][j] = modifyArr[arrIter][0];
                tempSplitSumArr = tempSplitSumArr - modifyArr[arrIter][0];
                modifyArr[arrIter][1]--;


                if(tempSplitSumArr == 0){
                    arrIter = 0; //problem place
                    break;
                }
            }
        }
        for (int[] ints : resultArr) {
            System.out.println(Arrays.toString(ints));
        }
        return true;
    }

    public static int arrSum(int[] a){
        int sum = 0;
        for (int j : a) sum += j;
        return sum;
    }


    public static void main(String[] args) {
        arraySplit(new int[]{2,1,3, 6, 12, 6, 2,1,3,2,1,3,2,1,3}, 3);
    }
}
