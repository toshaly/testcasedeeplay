package com.deeplay.test;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class ArrayTest {

    public static int[] arrayMagic(int arraySize, int maxValue){
        //create random arr
        Random random = new Random();
        return random.ints(arraySize, 0,maxValue+1).toArray();
    }

    public static int[] arraySort(int[] array){
        //time to split
        int[] evenNum = new int[array.length];
        int[] oddNum = new int[array.length];
        int zerosCount = 0;
        int j = 0, k = 0;
        for (int i = 0; i < array.length; i++) {
           if(array[i] % 2 == 0 && array[i] != 0){
               evenNum[j] = array[i];
               j++;
           }else if(array[i] % 2 != 0){
               oddNum[k] = array[i];
               k++;
           }else zerosCount++;
        }
        int[] newEvenNum = Arrays.copyOfRange(evenNum,0, j);
        int[] newOddNum = Arrays.copyOfRange(oddNum,0, k);

        //time to sort
        bubbleSort(newEvenNum,false);
        bubbleSort(newOddNum,true);
        //time to merge
        int[] finalArr = new int[newEvenNum.length + newOddNum.length + zerosCount];
        for (int i = 0; i < newOddNum.length; i++) {
            finalArr[i] = newOddNum[i];
        }
        for (int i = newOddNum.length + zerosCount, q = 0; i < finalArr.length ; i++) {
            finalArr[i] = newEvenNum[q];
            q++;
        }
        return finalArr;
    }

    public static void getPopularElement(int[] a) {
        int[] sortedArr = a;
        bubbleSort(sortedArr,true);
        //find all uniq elements
        int iter = 0;
        int[][] tempResultArr = new int[sortedArr.length][2];
        int[] item = new int[2];
        item[0] = sortedArr[0];
        item[1] = 1;
        tempResultArr[0] = item;
        for (int i = 1; i < sortedArr.length; i++) {
            if(tempResultArr[iter][0] == sortedArr[i]){
                tempResultArr[iter][1]++;
            } else {
                iter++;
                tempResultArr[iter][0] = sortedArr[i];
                tempResultArr[iter][1] = 1;
            }
        }
        int[][] resultArr = Arrays.copyOfRange(tempResultArr,0, iter+1);
        int maxRepeats = resultArr[0][1];
        for (int[] ints : resultArr) {
            System.out.println(Arrays.toString(ints));
            if (ints[1] >= maxRepeats){
                maxRepeats = ints[1];
            }
        }

        for (int[] ints : resultArr) {
            if (ints[1] == maxRepeats){
                System.out.println(ints[0] +" repeats " + ints[1] + " times");
            }
    }
    }


    public static void bubbleSort(int[] arr, boolean smallestToBiggest){
        boolean isSorted = false;
        while (!isSorted){
            isSorted = true;
            for (int i = 1; i < arr.length; i++) {
                if(smallestToBiggest ? arr[i] < arr[i - 1] : arr[i] > arr[i - 1]){
                    int temp = arr[i];
                    arr[i] = arr[i-1];
                    arr[i-1] = temp;
                    isSorted = false;
                }
            }
        }
    }


    public static void main(String[] args) {
        System.out.println("Input the size of the array and max value:");
        Scanner scanner = new Scanner(System.in);
        int arraySize = scanner.nextInt();
        int maxValue = scanner.nextInt();
        int[] finalArray = arrayMagic(arraySize, maxValue);
        System.out.println("Your array now");
        System.out.println(Arrays.toString(finalArray));
        System.out.println("Your array after transformation");
        System.out.println(Arrays.toString(arraySort(finalArray)));
        System.out.println("Now we find the most popular elements");
        getPopularElement(finalArray);
    }
}
